
package com.pucrs.tf.gui;

import com.pucrs.tf.business.*;
import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author guilherme_tomas
 */
public class ParkingMeter extends javax.swing.JFrame {

    private String inputData;
    private String inputHour;
    private String finalLeaveDate;

    /**
     * Creates new form Teste
     */
    public ParkingMeter() {

        //inicia os componentes conforme pre configurado
        initComponents();
        paymentType.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Moeda", "Cartão Recarregável", "Cartão Residente"}));
        setInputDate();
        setInputHour();
        currentHour.setText(getInputHour());
        currentDate.setText(getInputDate());
        setFinalLeave();
        //utiliza o singleton pra configurar os valores
        coinValue.setText(Helper.getInstance().toDecimalFormat(ConfigFile.getInstance().getTypeCoin(), "0.00"));
        totalValue.setText(ConfigFile.getInstance().getMinimalValue().toString());
        //utiliza o singleton pra configurar o endereço; ID e moedas
        parkingMeterAddress.setText(ConfigFile.getInstance().getAddress());
        idParkingMeter.setText(ConfigFile.getInstance().getParkingMeterId()+"");
        quantityCoin.setText(0+"");
    }

    
    public String getInputDate() {
        return inputData;
    }

    public void setInputDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        this.inputData = dateFormat.format(cal.getTime());
    }

    public String getInputHour() {
        return inputHour;
    }

    public void setInputHour() {
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        this.inputHour = hourFormat.format(cal.getTime());
    }
    
    public void setFinalLeave(){
        int cont = Integer.parseInt(finalHour.getText().trim()) + (ConfigFile.getInstance().getMinimalTime());
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
        Date data = null;
        try {
            data = formato.parse(currentDate.getText()+" "+ currentHour.getText());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.add(Calendar.SECOND, cont*60);
        finalLeave.setText(formatoHora.format(calendar.getTime()).toString());
        finalHour.setText(cont+"");
    }

    public void setFinalLeaveDate(String finalLeaveDate) {
        this.finalLeaveDate = finalLeaveDate;
    }
    
    @SuppressWarnings("unchecked")
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        qtdLabel = new javax.swing.JLabel();
        coinsLabel = new javax.swing.JLabel();
        coinLabel = new javax.swing.JLabel();
        totalValueLabel = new javax.swing.JLabel();
        addressLabel = new javax.swing.JLabel();
        idParkingMeterLabel = new javax.swing.JLabel();
        paymentFormLabel = new javax.swing.JLabel();
        cardTypeLabel = new javax.swing.JLabel();
        codeLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();

        decreaseButton = new javax.swing.JButton();
        increaseButton = new javax.swing.JButton();
        finishButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        paymentType = new javax.swing.JComboBox();
        currentDate = new javax.swing.JTextField();
        currentHour = new javax.swing.JTextField();
        finalHour = new javax.swing.JTextField();
        finalLeave = new javax.swing.JTextField();
        coinValue = new javax.swing.JTextField();
        quantityCoin = new javax.swing.JTextField();
        totalValue = new javax.swing.JTextField();
        parkingMeterAddress = new javax.swing.JTextField();
        idParkingMeter = new javax.swing.JTextField();
        cardIdentifier = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setText("Data atual:");

        currentDate.setEditable(false);
        currentDate.setBackground(new java.awt.Color(255, 255, 255));
        currentDate.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        currentDate.addActionListener(evt -> currentDateActionPerformed(evt));

        jLabel3.setText("Hora atual:");

        currentHour.setEditable(false);
        currentHour.setBackground(new java.awt.Color(255, 255, 255));
        currentHour.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        currentHour.addActionListener(evt -> currentHourActionPerformed(evt));

        decreaseButton.setText("-");
        decreaseButton.addActionListener(evt -> decreaseButtonActionPerformed(evt));

        finalHour.setEditable(false);
        finalHour.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        finalHour.setText("0");
        finalHour.setToolTipText("");

        increaseButton.setText("+");
        increaseButton.addActionListener(evt -> increaseButtonActionPerformed(evt));

        finishButton.setBackground(new java.awt.Color(0, 204, 51));
        finishButton.setText("Concluir");
        finishButton.addActionListener(evt -> finishButtonActionPerformed(evt));

        cancelButton.setBackground(new java.awt.Color(255, 0, 0));
        cancelButton.setText("Cancelar");

        paymentType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecionar", "Moeda", "Cartão Recarregável", "Cartão Residente" }));
        paymentType.addItemListener((java.awt.event.ItemEvent evt) -> paymentTypeItemStateChanged(evt));
        paymentType.addActionListener((java.awt.event.ActionEvent evt) -> paymentTypeActionPerformed(evt));

        timeLabel.setText("Tempo:");

        finalLeave.setEditable(false);
        finalLeave.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        coinValue.setEditable(false);
        coinValue.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        qtdLabel.setText("Quantidade:");

        coinsLabel.setText("Moedas de(R$):");

        coinLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        coinLabel.setText("Moeda");

        totalValue.setEditable(false);
        totalValue.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        totalValue.addActionListener((java.awt.event.ActionEvent evt) -> totalValueActionPerformed(evt));

        totalValueLabel.setText("Valor Total(R$):");

        parkingMeterAddress.setEditable(false);
        parkingMeterAddress.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        parkingMeterAddress.addActionListener((java.awt.event.ActionEvent evt) -> parkingMeterAddressActionPerformed(evt));

        addressLabel.setText("Endereço:");

        idParkingMeter.setEditable(false);
        idParkingMeter.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        idParkingMeter.addActionListener((java.awt.event.ActionEvent evt) -> idParkingMeterActionPerformed(evt));

        idParkingMeterLabel.setText("ID Parquímetro:");
        paymentFormLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        paymentFormLabel.setText("Forma de pagamento:");
        cardTypeLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cardTypeLabel.setText("Cartão Recarregável ou Residente");
        codeLabel.setText("Código:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(coinLabel)
                        .addGap(744, 744, 744))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(paymentFormLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(paymentType, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator1)
                            .addComponent(jSeparator2)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSeparator5)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(qtdLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(quantityCoin, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(48, 48, 48)
                                        .addComponent(coinsLabel)
                                        .addGap(18, 18, 18)
                                        .addComponent(coinValue))
                                    .addComponent(jSeparator6)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(cardTypeLabel)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(codeLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(cardIdentifier, javax.swing.GroupLayout.PREFERRED_SIZE, 645, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(18, 18, 18)
                                        .addComponent(currentDate))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(18, 18, 18)
                                        .addComponent(currentHour, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(timeLabel)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(decreaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(finalHour, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(increaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(finalLeave)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(idParkingMeterLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(idParkingMeter, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(22, 22, 22)
                                .addComponent(addressLabel)
                                .addGap(18, 18, 18)
                                .addComponent(parkingMeterAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(totalValueLabel)
                                .addGap(18, 18, 18)
                                .addComponent(totalValue))
                            .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(74, 74, 74))))
            .addGroup(layout.createSequentialGroup()
                .addGap(346, 346, 346)
                .addComponent(finishButton, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totalValue, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totalValueLabel)
                    .addComponent(parkingMeterAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addressLabel)
                    .addComponent(idParkingMeter, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idParkingMeterLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(currentDate, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(currentHour, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(finalHour, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(increaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(decreaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(timeLabel)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(finalLeave, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(paymentFormLabel)
                    .addComponent(paymentType, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(coinLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(quantityCoin, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtdLabel)
                    .addComponent(coinsLabel)
                    .addComponent(coinValue, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(cardTypeLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cardIdentifier, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(codeLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(finishButton, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }

    private void currentDateActionPerformed(java.awt.event.ActionEvent evt) {

    }

    private void currentHourActionPerformed(java.awt.event.ActionEvent evt) {

    }
    
    private void raiseValue(){
        Double value = Double.parseDouble(totalValue.getText().replace(',','.'));
        value += ConfigFile.getInstance().getValue();
        totalValue.setText(value.toString());
    }
    
    private void decreaseValue(){
        Double value = Double.parseDouble(totalValue.getText().replace(',','.'));
        value -= ConfigFile.getInstance().getValue();
        totalValue.setText(value.toString());
    }

    private void increaseButtonActionPerformed(java.awt.event.ActionEvent evt) {
        int minutes = Integer.parseInt(finalHour.getText().trim());
        Integer count;

        if (minutes >= ConfigFile.getInstance().getMaxTime()) {
            count = ConfigFile.getInstance().getMaxTime();
            totalValue.setText(ConfigFile.getInstance().getMaximumValue().toString());
        }
        else {
            count = minutes + ConfigFile.getInstance().getIncrement();
            raiseValue();
        }

        String finalLeaveDate = getFinalLeaveDate(count);
        setFinalLeaveDate(finalLeaveDate);
        finalHour.setText(count.toString());
    }

    private void decreaseButtonActionPerformed(java.awt.event.ActionEvent evt) {
        int minutes = Integer.parseInt(finalHour.getText().trim());
        Integer count;

        if (minutes <= ConfigFile.getInstance().getMinimalTime()) {
            count = ConfigFile.getInstance().getMinimalTime();
            totalValue.setText(ConfigFile.getInstance().getMinimalValue().toString());
        }
        else {
            count = minutes - ConfigFile.getInstance().getIncrement();
            decreaseValue();
        }

        String finalLeaveDate = getFinalLeaveDate(count);
        setFinalLeaveDate(finalLeaveDate);
        finalHour.setText(count.toString());
    }

    private String getFinalLeaveDate(int count) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        String finalLeaveDate = currentDate.getText()+" "+hourFormat.format(calendar.getTime()).toString();
        Date date = null;
        try {
            date = dateFormat.parse(currentDate.getText()+" "+ currentHour.getText());
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        calendar.setTime(date);
        calendar.add(Calendar.SECOND, count*60);
        finalLeave.setText(hourFormat.format(calendar.getTime()).toString());
        return finalLeaveDate;
    }

    private void paymentTypeActionPerformed(java.awt.event.ActionEvent evt) {

    }

    private void paymentTypeItemStateChanged(java.awt.event.ItemEvent evt) {
    }

    private void finishButtonActionPerformed(java.awt.event.ActionEvent evt) {
        
        ConfigFile.getInstance().setParkingMeterId(Integer.parseInt(idParkingMeter.getText()));
        ConfigFile.getInstance().setAddress(parkingMeterAddress.getText());
        
        switch(paymentType.getSelectedIndex()){
            case 0:
                paymentCoin();
                break;
            case 1:
                paymentRechargeableCard();
                break;
            case 2:
                paymentResidentCard();
                break;
            default:
                paymentCoin();
        }
        
    }
    
    private void paymentCoin(){
        if(Integer.parseInt(quantityCoin.getText().trim()) * ConfigFile.getInstance().getTypeCoin() < Double.parseDouble(totalValue.getText())) {
            Helper.getInstance().setPopUp("Valor inferior ao total.", "");
        }
        else {
            setConfigurationsParkingMeter();
            
            //Utiliza fachada
            OperationFacade of = new OperationFacade();
            Ticket ticket = of.doPayment(new Coin(Integer.parseInt(quantityCoin.getText()), Double.parseDouble(Helper.getInstance().convertToFloat(totalValue.getText()))));
            double change = of.requestChange();
            int numberCoins = convertChange(change, ConfigFile.getInstance().getTypeCoin());
            Helper.getInstance().setPopUp(ticket +"\n\n Troco: +"+ numberCoins+" de R$"+Helper.getInstance().toDecimalFormat(ConfigFile.getInstance().getTypeCoin(), "0.00"), "Ticket gerado com sucesso!");
        }
    }
    
    private int convertChange(double change, double coinType){
        int numberCoins = 0;
        if(change <= coinType)
            numberCoins = ((Double)(change/coinType)).intValue();
        else{
            numberCoins =  ((Double)(change / coinType)).intValue();
        }
        return numberCoins;
    }
    
    private void paymentRechargeableCard(){
        if(StringUtils.isEmpty(cardIdentifier.getText())) {
            Helper.getInstance().setPopUp("Digite o código do cartão.", "");
            return;
        }
        //Configuracoes do parquimetro
        setConfigurationsParkingMeter();

        //Utiliza fachada
        OperationFacade of = new OperationFacade();
        Ticket ticket = of.doPayment(new RechargeableCard(cardIdentifier.getText().trim(), Double.parseDouble(ConfigFile.getInstance().getMinimalValue()+"")));
        Helper.getInstance().setPopUp(ticket.toString(), "Ticket gerado com sucesso!");
    }
    
    private void paymentResidentCard(){
        if(StringUtils.isEmpty(cardIdentifier.getText())) {
            Helper.getInstance().setPopUp("Digite o código do cartão.", "");
            return;
        }
        //Configuracoes do parquimetro
        setConfigurationsParkingMeter();

        //Utiliza fachada
        OperationFacade of = new OperationFacade();
        Ticket ticket = of.doPayment(new ResidentCard(cardIdentifier.getText().trim(), Double.parseDouble(Helper.getInstance().convertToFloat(totalValue.getText()))));
        Helper.getInstance().setPopUp(ticket.toString(), "Ticket gerado com sucesso!");
    }
    private void setConfigurationsParkingMeter(){
        ParkingMeterConfiguration.getInstance().setEmissionDateTime(currentDate.getText()+" "+ currentHour.getText());
        ParkingMeterConfiguration.getInstance().setValidToDateTime(currentDate.getText()+" "+ finalLeave.getText());
        ParkingMeterConfiguration.getInstance().setParkingMeterAddress(ConfigFile.getInstance().getAddress());
        ParkingMeterConfiguration.getInstance().setParkingMeterId(ConfigFile.getInstance().getParkingMeterId());
    }
    
    private void totalValueActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void parkingMeterAddressActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void idParkingMeterActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }


    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ParkingMeter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ParkingMeter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ParkingMeter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ParkingMeter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        if (!StringUtils.isEmpty(args[0])) {
            ConfigFile.getInstance(args[0]);
        }

        java.awt.EventQueue.invokeLater(() -> new ParkingMeter().setVisible(true));
    }

    private javax.swing.JButton increaseButton;
    private javax.swing.JButton decreaseButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton finishButton;
    private javax.swing.JTextField currentDate;
    private javax.swing.JTextField parkingMeterAddress;
    private javax.swing.JTextField currentHour;
    private javax.swing.JTextField finalHour;
    private javax.swing.JTextField idParkingMeter;
    private javax.swing.JTextField cardIdentifier;
    private javax.swing.JLabel idParkingMeterLabel;
    private javax.swing.JLabel paymentFormLabel;
    private javax.swing.JLabel cardTypeLabel;
    private javax.swing.JLabel codeLabel;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JLabel qtdLabel;
    private javax.swing.JLabel coinsLabel;
    private javax.swing.JLabel coinLabel;
    private javax.swing.JLabel totalValueLabel;
    private javax.swing.JLabel addressLabel;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextField quantityCoin;
    private javax.swing.JTextField finalLeave;
    private javax.swing.JComboBox paymentType;
    private javax.swing.JTextField coinValue;
    private javax.swing.JTextField totalValue;
}
