package com.pucrs.tf.gui;

import com.pucrs.tf.business.ConfigFile;

/**
 *
 * @author guilherme_tomas
 */
public class App {
    public static void main(String[] args) throws Exception {
        ConfigFile.getInstance(args[0]);
        new ParkingMeter().setVisible(true);
        ConfigFile.getInstance(args[1]);
        new ParkingMeter().setVisible(true);
    }
}
