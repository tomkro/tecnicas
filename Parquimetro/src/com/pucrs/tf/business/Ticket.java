package com.pucrs.tf.business;

/**
 *
 * @author guilherme_tomas
 */
public class Ticket {

    private String serialNumber;
    private int parkingMeterId;
    private String parkingMeterAddress;
    private String emissionDateTime;
    private String validToDateTime;
    private String exemption;
    private int paymentType;
    private double value;

    public Ticket() {

        if (!TicketValidator.getInstance().validateParkingMeterId(ParkingMeterConfiguration.getInstance().getParkingMeterId()))
            throw new IllegalArgumentException("Identificador do Parquímetro inválido.");        
        if (!TicketValidator.getInstance().validateAddress(ParkingMeterConfiguration.getInstance().getParkingMeterAddress()))
            throw new IllegalArgumentException("Endereço do Parquímetro inválido.");        
        if (!TicketValidator.getInstance().validateDate(ParkingMeterConfiguration.getInstance().getEmissionDateTime()))
            throw new IllegalArgumentException("Data inválida.");        
        if (!TicketValidator.getInstance().validateDate(ParkingMeterConfiguration.getInstance().getEmissionDateTime()))
            throw new IllegalArgumentException("Data inválida.");        
        
        this.parkingMeterId = ParkingMeterConfiguration.getInstance().getParkingMeterId();
        this.parkingMeterAddress = ParkingMeterConfiguration.getInstance().getParkingMeterAddress();
        this.emissionDateTime = ParkingMeterConfiguration.getInstance().getEmissionDateTime();
        this.validToDateTime = ParkingMeterConfiguration.getInstance().getValidToDateTime();;
    }

    public int getParkingMeterId() {
        return parkingMeterId;
    }

    public String getParkingMeterAddress() {
        return parkingMeterAddress;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getEmissionDateTime() {
        return emissionDateTime;
    }

    public void setParkingMeterId(int parkingMeterId) {
        this.parkingMeterId = parkingMeterId;
    }

    public void setParkingMeterAddress(String parkingMeterAddress) {
        this.parkingMeterAddress = parkingMeterAddress;
    }

    public void setEmissionDateTime(String emissionDateTime) {
        this.emissionDateTime = emissionDateTime;
    }

    public void setValidToDateTime(String validToDateTime) {
        this.validToDateTime = validToDateTime;
    }

    public String getValidToDateTime() {
        return validToDateTime;
    }

    public String getExemption() {
        return exemption;
    }

    public void setExemption(String exemption) {
        if (!TicketValidator.getInstance().validateExemption(exemption))
            throw new IllegalArgumentException("Isenção inválida.");       
        this.exemption = exemption;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        if (!TicketValidator.getInstance().validatePaymentType(paymentType))
            throw new IllegalArgumentException("Tipo de Pagamento inválido.");
        this.paymentType = paymentType;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(double value) {
        if (!TicketValidator.getInstance().validateValue(value))
            throw new IllegalArgumentException("Valor inválido."); 
        this.value = value;
    }

    @Override
    public String toString() {
        String retorno = "Parquímetro número: " + getParkingMeterId() + "\n"
                + "Endereço: " + getParkingMeterAddress() + "\n"
                + "Serial: " + getSerialNumber() + "\n"
                + "Valor: " + Helper.getInstance().toDecimalFormat(getValue(), "0.00") + "\n"
                + "Data emitada: " + getEmissionDateTime() + "\n"
                + "Data de validade: " + getValidToDateTime() + "\n"
                + "Tipo de Pagamento: " + Helper.getInstance().getTipoDePagamento(getPaymentType());
        return retorno;
    }
}
