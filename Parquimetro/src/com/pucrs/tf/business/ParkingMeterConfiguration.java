package com.pucrs.tf.business;

/**
 * @author guilherme_tomas
 */
public class ParkingMeterConfiguration {

    public static ParkingMeterConfiguration p;
    private int parkingMeterId;
    private String parkingMeterAddress;
    private String emissionDateTime;
    private String validToDateTime;

    private ParkingMeterConfiguration() {
    }

    public static ParkingMeterConfiguration getInstance() {
        if (ParkingMeterConfiguration.p == null)
            ParkingMeterConfiguration.p = new ParkingMeterConfiguration();
        return ParkingMeterConfiguration.p;
    }

    public int getParkingMeterId() {
        return parkingMeterId;
    }

    public void setParkingMeterId(int parkingMeterId) {
        this.parkingMeterId = parkingMeterId;
    }

    public String getParkingMeterAddress() {
        return parkingMeterAddress;
    }

    public void setParkingMeterAddress(String parkingMeterAddress) {
        this.parkingMeterAddress = parkingMeterAddress;
    }

    public String getEmissionDateTime() {
        return emissionDateTime;
    }

    public void setEmissionDateTime(String emissionDateTime) {
        this.emissionDateTime = emissionDateTime;
    }

    public String getValidToDateTime() {
        return validToDateTime;
    }

    public void setValidToDateTime(String validToDateTime) {
        this.validToDateTime = validToDateTime;
    }

    @Override
    public String toString() {
        return "ParkingMeter{" + "parkingMeterId=" + parkingMeterId + ", parkingMeterAddress=" + parkingMeterAddress + ", emissionDateTime=" + emissionDateTime + ", validToDateTime=" + validToDateTime + '}';
    }
}
