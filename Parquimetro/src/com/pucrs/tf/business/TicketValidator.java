package com.pucrs.tf.business;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author guilherme_tomas
 */
public class TicketValidator {

    private static TicketValidator vt = null;

    private TicketValidator() {
    }

    public static TicketValidator getInstance() {
        if (vt == null) {
            vt = new TicketValidator();
        }
        return (vt);
    }

    public boolean validateParkingMeterId(int id) {
        if (id <= 0) {
            return false;
        }
        return true;
    }
        
    public boolean validateAddress(String address) {
        return !StringUtils.isEmpty(address);
    }
    
    public boolean validateDate(String data) {
        return data.contains("/") && data.contains(":") && !StringUtils.isEmpty(data);
    }
    
    public boolean validateExemption(String exemption) {
        return !StringUtils.isEmpty(exemption) && exemption.length() <= 1;
    }
    
    public boolean validatePaymentType(int type) {
        return !(type < 1 || type > 3);
    }
    
    public boolean validateValue(double value) {
        return value >= 0;
    }
}
