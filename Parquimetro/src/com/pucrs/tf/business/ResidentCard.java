package com.pucrs.tf.business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author guilherme_tomas
 */
public class ResidentCard extends Payment {

    private String id;

    private String name;

    private String exemption;
    public ResidentCard(String id, double value) {

        if(!PaymentValidator.getInstance().cardValidator(id))
            throw new IllegalArgumentException("Número de cartão inválido!");
        if(!PaymentValidator.getInstance().valueValidator(value))
            throw new IllegalArgumentException("Valor inválido!");

        this.id = id;
        setValue(value);
        setPaymentType(3);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExemption() {
        return exemption;
    }

    public void setExemption(String exemption) {
        this.exemption = exemption;
    }

    @Override
    public void doPayment(){
        setExemption("S");
    }

    @Override
    public String toString() {
        return "ResidentCard{" + "id=" + id + ", name=" + name + ", exemption=" + exemption + '}';
    }
}
