/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.business;

/**
 *
 * @author guilherme_tomas
 */
public class TicketException extends Exception{
    public TicketException(Throwable cause) {
        super(cause);
    }

    public TicketException(String message, Throwable cause) {
        super(message, cause);
    }

    public TicketException(String message) {
        super(message);
    }

    public TicketException() {
    }
}
