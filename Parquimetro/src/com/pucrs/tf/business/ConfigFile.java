/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.business;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 *
 * @author guilherme_tomas
 */
public class ConfigFile {
    
    private int parkingMeterId;
    private String address;
    private int minimalTime;
    private int increment;
    private int maxTime;
    private double value;
    private int startTimeHour;
    private int startTimeMinute;
    private int finalTimeHour;
    private int finalTimeMinute;
    private double typeCoin;
    private double minimalValue;
    private double maximumValue;
    private double cardBalance;
    private static ConfigFile arq = null;
    private static String DEFAULT_CONFIGURATION_FILE = "configuracoes1.txt";

    
    private ConfigFile(String narq) {
        setConfiguration(narq);
    }
    
    //Singleton
    public static ConfigFile getInstance(){
        if (arq == null){
            getInstance(DEFAULT_CONFIGURATION_FILE);
        }
        return(arq);
    }

    public static ConfigFile getInstance(String narq){
        arq = new ConfigFile(narq);
        return arq;
    }

    public int getParkingMeterId() {
        return parkingMeterId;
    }

    public void setParkingMeterId(int parkingMeterId) {
        this.parkingMeterId = parkingMeterId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMinimalTime() {
        return minimalTime;
    }

    public void setMinimalTime(int minimalTime) {
        this.minimalTime = minimalTime;
    }

    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getStartTimeHour() {
        return startTimeHour;
    }

    public void setStartTimeHour(int startTimeHour) {
        this.startTimeHour = startTimeHour;
    }

    public int getStartTimeMinute() {
        return startTimeMinute;
    }

    public void setStartTimeMinute(int startTimeMinute) {
        this.startTimeMinute = startTimeMinute;
    }

    public int getFinalTimeHour() {
        return finalTimeHour;
    }

    public void setFinalTimeHour(int finalTimeHour) {
        this.finalTimeHour = finalTimeHour;
    }

    public int getFinalTimeMinute() {
        return finalTimeMinute;
    }

    public void setFinalTimeMinute(int finalTimeMinute) {
        this.finalTimeMinute = finalTimeMinute;
    }

    public Double getTypeCoin() {
        return typeCoin;
    }

    public void setTypeCoin(double typeCoin) {
        this.typeCoin = typeCoin;
    }

    public Double getMinimalValue() {
        return minimalValue;
    }

    public void setMinimalValue(double minimalValue) {
        this.minimalValue = minimalValue;
    }

    public Double getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(double maximumValue) {
        this.maximumValue = maximumValue;
    }

    public double getCardBalance() {
        return cardBalance;
    }

    public void setCardBalance(double cardBalance) {
        this.cardBalance = cardBalance;
    }

    /**
     *
     * @param narq
     */
    public void setConfiguration(String narq) {
        Path path = Paths.get(narq);
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.defaultCharset()))) {

            sc.useDelimiter("[;\\n]");
            while (sc.hasNext()) {
                setParkingMeterId(Integer.parseInt(sc.next()));
                setAddress(sc.next());
                setTypeCoin(Double.parseDouble(sc.next()));
                setMinimalTime(Integer.parseInt(sc.next()));
                setIncrement(Integer.parseInt(sc.next()));
                setMaxTime(Integer.parseInt(sc.next()));
                setValue(Double.parseDouble(sc.next()));
                setStartTimeHour(Integer.parseInt(sc.next()));
                setStartTimeMinute(Integer.parseInt(sc.next()));
                setFinalTimeHour(Integer.parseInt(sc.next()));
                setFinalTimeMinute(Integer.parseInt(sc.next()));
                setMinimalValue(Double.parseDouble(sc.next()));
                setMaximumValue(Double.parseDouble(sc.next()));
                setCardBalance(Double.parseDouble(sc.next()));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "ConfigFile{" + "parkingMeterId=" + parkingMeterId + ", address=" + address + ", minimalTime=" + minimalTime + ", increment=" + increment + ", maxTime=" + maxTime + ", value=" + value + ", startTimeHour=" + startTimeHour + ", startTimeMinute=" + startTimeMinute + ", finalTimeHour=" + finalTimeHour + ", finalTimeMinute=" + finalTimeMinute + ", typeCoin=" + typeCoin + ", minimalValue=" + minimalValue + ", maximumValue=" + maximumValue + ", cardBalance=" + cardBalance + '}';
    }
}
