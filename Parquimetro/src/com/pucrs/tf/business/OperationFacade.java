package com.pucrs.tf.business;

import com.pucrs.tf.persistence.DAOException;
import com.pucrs.tf.persistence.ITicketDAO;
import com.pucrs.tf.persistence.TicketDAOLogFile;

/**
 *
 * @author guilherme_tomas
 */
public class OperationFacade {
    
    private static Payment payment;

    public OperationFacade() {}
    
    public Ticket doPayment(Payment p){
        this.payment = p;
        try {
            this.payment.doPayment();
        } catch (PaymentException e) {
            System.out.println(e.getMessage());
        }        
        return generateTicket();
    }
    

    public double requestChange(){
        double valor = 0.0;
        try {
            valor = ((Coin)this.payment).getChange();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return valor;
    }
    

    public Ticket generateTicket(){
        
        ITicketDAO ticketDB = new TicketDAOLogFile();
        Ticket ticket = new Ticket();
        String sn = ticketDB.getLastTicketId();
        ticket.setSerialNumber(sn);
        ticket.setPaymentType(this.payment.getPaymentType());
        ticket.setExemption((ticket.getPaymentType() == 3 ? "S" : "N"));
        ticket.setValue(this.payment.getValue());
        try {    
            ticketDB.insert(ticket);
        } catch (DAOException e) {
            System.out.println(e.getMessage());
        }                
        return ticket;
    }
    
}
