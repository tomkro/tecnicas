package com.pucrs.tf.business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author guilherme_tomas
 */
public class Coin extends Payment {
    
    private int amount;
    private double change;

    public Coin(int amount, double change) {
        
        if(!PaymentValidator.getInstance().quantityValidator(amount))
            throw new IllegalArgumentException("Quantidade inválida!");        
        if(!PaymentValidator.getInstance().valueValidator(change))
            throw new IllegalArgumentException("Valor inválido!");


        this.amount = amount;
        super.setValue(change);
        setPaymentType(1);
    }
        
    @Override
    public void doPayment(){
        double totalValue = getValue();
        double paidValue = getAmount() * ConfigFile.getInstance().getTypeCoin();
        setChange(paidValue - totalValue);
    }
    
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    @Override
    public String toString() {
        return "Coin{" + "amount=" + amount + ", change=" + change + '}';
    }
}
