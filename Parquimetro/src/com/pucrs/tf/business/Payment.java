package com.pucrs.tf.business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author guilherme_tomas
 */
public abstract class Payment implements IPayment {
    
    private int paymentType;
    private double valor;
    
    public double getValue() {
        return valor;
    }

    public void setValue(double valor) {
        this.valor = valor;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int tipoPagamento) {
        this.paymentType = tipoPagamento;
    }
}
