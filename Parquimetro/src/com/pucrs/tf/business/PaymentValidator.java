package com.pucrs.tf.business;

/**
 *
 * @author guilherme_tomas
 */
public class PaymentValidator {

    private static PaymentValidator vp = null;

    private PaymentValidator() {
    }

    public static PaymentValidator getInstance() {
        if (vp == null) {
            vp = new PaymentValidator();
        }
        return (vp);
    }
    
    public boolean valueValidator(double value) {
        return value > 0;
    }
    
    public boolean quantityValidator(int qtd) {
        return qtd > 0;
    }
        
    public boolean cardValidator(String card) {
        return card.length() == 128;
    }
}
