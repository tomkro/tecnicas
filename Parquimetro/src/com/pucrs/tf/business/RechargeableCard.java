package com.pucrs.tf.business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author guilherme_tomas
 */
public class RechargeableCard extends Payment {
    
    private String id;
    private double balance;
    private String name;

    public RechargeableCard(String id, double valor) {
        
        if(!PaymentValidator.getInstance().cardValidator(id))
            throw new IllegalArgumentException("Número de cartão inválido!");
        if(!PaymentValidator.getInstance().valueValidator(valor))
            throw new IllegalArgumentException("Valor inválido!");
        
        this.id = id;
        this.balance = ConfigFile.getInstance().getCardBalance();
        setValue(valor);
        setPaymentType(2);
    }
    
    public String getNumber() {
        return id;
    }

    public void setNumber(String id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
        
    @Override
    public void doPayment(){
        setBalance(getBalance() - getValue());
    }

    @Override
    public String toString() {
        return "RechargeableCard{" + "id=" + id + ", balance=" + balance + ", name=" + name + '}';
    }
}
