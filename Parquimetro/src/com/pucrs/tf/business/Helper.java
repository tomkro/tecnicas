package com.pucrs.tf.business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author guilherme_tomas
 */
public class Helper {

    public static Helper h;

    private Helper() {
    }

    public static Helper getInstance() {
        if (Helper.h == null) {
            Helper.h = new Helper();
        }

        return Helper.h;
    }

    public java.sql.Date formatDate(String d) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(d);
            String output = dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return this.convertToSqlDate(date);
    }

    public java.sql.Date convertToSqlDate(Date d) {
        java.util.Date utilDate = d;
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }

    public String getRandomHexString() {
        int numchars = 128;
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while (sb.length() < numchars) {
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, numchars);
    }

    public void setPopUp(String infoMessage, String titleBar) {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

    public String toDecimalFormat(double data, String format) {
        String priceString = new DecimalFormat(format).format(data);
        return priceString;
    }

    public String formatDateTime(String date) {
        String[] data_hora = date.split(" ");
        String[] data = data_hora[0].split("/");
        return data[2] + "-" + data[1] + "-" + data[0] + " " + data_hora[1];
    }

    public String convertToDate(String date) {
        String[] data_hora = date.split(" ");
        String[] data = data_hora[0].split("/");
        return data[2] + "-" + data[1] + "-" + data[0];
    }
    
    public String convertToFloat(String valor){
        return valor.replace(",", ".");
    }
    public String getTipoDePagamento(int tipo) {
        String tipoDePagamento = null;
        switch (tipo) {
            case 1:
                tipoDePagamento = "Moeda";
                break;
            case 2:
                tipoDePagamento = "Cartão Recarregável";
                break;
            case 3:
                tipoDePagamento = "Cartão Redidente";
                break;
        }
        return tipoDePagamento;
    }
}
