/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.persistence;

import com.pucrs.tf.business.Ticket;

/**
 *
 * @author guilherme_tomas
 */
public interface ITicketDAO {
    boolean insert(Ticket t) throws DAOException;
    String getLastTicketId();
}
