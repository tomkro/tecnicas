

package com.pucrs.tf.persistence;

/**
 *
 * @author guilherme_tomas
 */
public class DAOException extends Exception {

    public DAOException(String message) {
        super(message);
    }

}
