package com.pucrs.tf.persistence;

import com.pucrs.tf.business.Ticket;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TicketDAOLogFile implements ITicketDAO {

    private final String LOG_FILE_TEMPLATE = "%s;%s;%s;%s;%s;%s;%s;\r\n";
    private final String LOG_FILE_NAME;
    private final String CONTROL_FILE_NAME = "control.config";



    public TicketDAOLogFile() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        Date date = new Date();
        String filename = String.format("log_%s.log", dateFormat.format(date));
        String path = "output/";
        createFile(filename, path, null);
        LOG_FILE_NAME =  path+filename;
        createFile(CONTROL_FILE_NAME, "", "00000");
    }

    private void createFile(String filename, String path, String defaultContent) {
        if(!Files.exists(Paths.get(path+filename))) {
            try {
                if(!StringUtils.isEmpty(path) && !Files.exists(Paths.get(path))) {
                    Files.createDirectory(Paths.get(path));
                }

                Files.createFile(Paths.get(path+filename));

                if(!StringUtils.isEmpty(defaultContent)) {
                    Files.write(Paths.get(path+filename),defaultContent.getBytes());
                }
            }
            catch (IOException e) {
                throw new RuntimeException("Não foi possível criar os arquivos de log");
            }
        }
    }

    @Override
    public boolean insert(Ticket t) throws DAOException {
        boolean flag = true;

        String entry = String.format(LOG_FILE_TEMPLATE,
                            t.getPaymentType(), t.getParkingMeterId(), t.getParkingMeterAddress(),
                            t.getEmissionDateTime(), t.getValidToDateTime(), t.getExemption(),
                            t.getValue().toString()
                        );

        try {
            Files.write(Paths.get(LOG_FILE_NAME), entry.getBytes(), StandardOpenOption.APPEND);
            Files.write(Paths.get(CONTROL_FILE_NAME), t.getSerialNumber().getBytes(), StandardOpenOption.WRITE);
        }
        catch (IOException e) {
            flag=false;
            e.printStackTrace();
        }

        return flag;
    }

    public String getLastTicketId() {
        try {
            Integer currentId = Integer.parseInt(new String(Files.readAllBytes(Paths.get(CONTROL_FILE_NAME))));
            currentId++;
            return StringUtils.leftPad(currentId.toString(), 5, '0');
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
