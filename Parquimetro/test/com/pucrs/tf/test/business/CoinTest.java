/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.test.business;


import com.pucrs.tf.business.Coin;
import com.pucrs.tf.business.DumpFile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author guilherme_tomas
 */
public class CoinTest {
    
    
    @Before
    public void setUp() throws Exception {
        DumpFile.getInstance("configuracoes1.txt");
        DumpFile.getInstance().setParkingMeterId(1);
    }
    
    /**
     *  Método para testar pagamento que não tem troco.
     */
    @Test
    public void testPagamentoSemTroco() {
        System.out.println("pagamento sem troco");
        Coin instance = new Coin(2, 1.0);
        instance.doPayment();
        assertEquals(0.0, instance.getChange(), 0.0);
    }
    
    /**
     * Método para testar troco de um pagamento.
     */
    @Test
    public void testPagamentoComTroco() {
        System.out.println("pagamento com troco");
        Coin instance = new Coin(4, 1.75);
        instance.doPayment();
        assertEquals(0.25, instance.getChange(), 0.0);
    }
    
    /**
     * Método para testar troco de um pagamento em moedas.
     */
    @Test
    public void testPagamentoComTrocoEmCoins() {
        System.out.println("pagamento com troco em moedas");
        Coin instance = new Coin(4, 1.75);
        instance.doPayment();
        int trocoEmCoins = convertChange(instance.getChange(), DumpFile.getInstance().getTypeCoin());
        assertEquals(1, trocoEmCoins);
    }
    
    /**
     * Método para converter troco em moedas.
     * @param change
     * @param coinType
     * @return 
     */
    private int convertChange(double change, double coinType){
        Double nroCoinsD = 0.0;
        if(change <= coinType)
            nroCoinsD = new Double(1.0);
        else{
            nroCoinsD = new Double(change / coinType);
        }
        return nroCoinsD.intValue();
    }
//    
    /**
     * Método para testar quantidade de moedas pagas.
     */
    @Test
    public void testGetAmount() {
        System.out.println("getAmount");
        Coin instance = new Coin(4, 1.75);
        instance.setAmount(3);
        int expResult = 3;
        int result = instance.getAmount();
        assertEquals(expResult, result);
    }
}
