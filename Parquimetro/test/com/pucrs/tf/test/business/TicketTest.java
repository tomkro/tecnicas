/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.test.business;


import com.pucrs.tf.business.DumpFile;
import com.pucrs.tf.business.ParkingMeterConfiguration;
import com.pucrs.tf.business.Ticket;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author guilherme_tomas
 */
public class TicketTest {
    
    private DumpFile conf;
    
    
    @Before
    public void setUp() throws Exception {
        conf.getInstance("configuracoes1.txt");
        conf.getInstance().setParkingMeterId(1);
        conf.getInstance().setAddress("Av. Ipiranga 226");
        conf.getInstance().setTypeCoin(0.50);
        conf.getInstance().setMinimalTime(30);
        conf.getInstance().setIncrement(10);
        conf.getInstance().setMaxTime(120);
        conf.getInstance().setValue(Double.parseDouble("0.25"));
        conf.getInstance().setStartTimeHour(Integer.parseInt("8"));
        conf.getInstance().setStartTimeMinute(Integer.parseInt("30"));
        conf.getInstance().setFinalTimeHour(Integer.parseInt("18"));
        conf.getInstance().setFinalTimeMinute(Integer.parseInt("30"));
        conf.getInstance().setMinimalValue(Double.parseDouble("0.75"));
        conf.getInstance().setMaximumValue(Double.parseDouble("3"));
        conf.getInstance().setCardBalance(Double.parseDouble("300"));
        
        ParkingMeterConfiguration.getInstance().setParkingMeterId(conf.getInstance().getParkingMeterId());
        ParkingMeterConfiguration.getInstance().setParkingMeterAddress(conf.getInstance().getAddress());
        ParkingMeterConfiguration.getInstance().setEmissionDateTime("01/03/2015 14:15:36");
        ParkingMeterConfiguration.getInstance().setValidToDateTime("01/03/2015 14:15:36");
    }
    
    /**
     * Método para testar data e hora passada por parâmetro.
     */
    @Test
    public void testGetEmissionDateTime() {
        System.out.println("getEmissionDateTime");
        Ticket instance = new Ticket();
        instance.setEmissionDateTime(ParkingMeterConfiguration.getInstance().getEmissionDateTime());
        String expResult = "01/03/2015 14:15:36";
        String result = instance.getEmissionDateTime();
        assertEquals(expResult, result);
    }

    /**
     * Método para testar data e hora passada por parâmetro.
     */
    @Test
    public void testGetDataHoraValidade() {
        System.out.println("getValidToDateTime");
        Ticket instance = new Ticket();
        instance.setValidToDateTime(ParkingMeterConfiguration.getInstance().getValidToDateTime());
        String expResult = "01/03/2015 14:15:36";
        String result = instance.getValidToDateTime();
        assertEquals(expResult, result);
    }

    /**
     * Método para testar isenção.
     */
    @Test
    public void testGetExemption() {
        System.out.println("getExemption");
        Ticket instance = new Ticket();
        instance.setExemption("S");
        String expResult = "S";
        String result = instance.getExemption();
        assertEquals(expResult, result);
    }

    /**
     * Método para testar isenção.
     */
    @Test
    public void testSetExemption() {
        System.out.println("setExemption");
        String exemption = "N";
        Ticket instance = new Ticket();
        instance.setExemption(exemption);
        assertEquals("N", instance.getExemption());
    }

    /**
     * Método para testar tipo de pagamento.
     */
    @Test
    public void testGetTipoPagamento() {
        System.out.println("getPaymentType");
        Ticket instance = new Ticket();
        instance.setPaymentType(1);
        int expResult = 1;
        int result = instance.getPaymentType();
        assertEquals(expResult, result);
    }

    /**
     * Método para trocar troco de tipo de pagamento
     */
    @Test
    public void testSetTipoPagamento() {
        System.out.println("setPaymentType");
        int tipoPagamento = 1;
        Ticket instance = new Ticket();
        instance.setPaymentType(tipoPagamento);
        assertEquals(tipoPagamento, instance.getPaymentType());
    }

    /**
     * Método para testar valor do pagamento.
     */
    @Test
    public void testGetValor() {
        System.out.println("getValue");
        Ticket instance = new Ticket();
        instance.setValue(5.0);
        double expResult = 5.0;
        double result = instance.getValue();
        assertEquals(expResult, result, 0.0);
    }    
}
