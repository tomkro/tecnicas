/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.test.business;

import com.pucrs.tf.business.ConfigFile;
import com.pucrs.tf.business.RechargeableCard;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author guilherme_tomas
 */
public class RechargeableCardTest {
    

    @Before
    public void setUp() throws Exception {
        ConfigFile.getInstance("configuracoes1.txt");
    }
    
    /**
     * Método para testar identificador do cartão.
     */
    @Test
    public void testGetNumero() {
        System.out.println("getNumber");
        RechargeableCard instance = new RechargeableCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        String expResult = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        String result = instance.getNumber();
        assertEquals(expResult, result);
    }

    /**
     * Método para testar identificador do cartão.
     */
    @Test
    public void testSetNumber() {
        System.out.println("setNumber");
        String id = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        RechargeableCard instance = new RechargeableCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        instance.setNumber(id);
        String expResult = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        assertEquals(expResult, instance.getNumber());
    }

    /**
     * Método para testar saldo no cartão.
     */
    @Test
    public void testGetSaldo() {
        System.out.println("getBalance");
        RechargeableCard instance = new RechargeableCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        instance.doPayment();
        double expResult = 290.0;
        double result = instance.getBalance();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Método para testar saldo no cartão.
     */
    @Test
    public void testGetSetBalance() {
        System.out.println("setBalance");
        RechargeableCard instance = new RechargeableCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        instance.doPayment();
        instance.setBalance(310.0);
        double expResult = 310.0;
        double result = instance.getBalance();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Método para testar nome no cartão.
     */
    @Test
    public void testGetSetName() {
        System.out.println("getName");
        RechargeableCard instance = new RechargeableCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        String expResult = "Eduardo Ouriques";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    }


    /**
     * Método para testar pagamento.
     */
    @Test
    public void testPayment() {
        System.out.println("doPayment");
        RechargeableCard instance = new RechargeableCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        instance.doPayment();
        double expResult = 290.0;
        double result = instance.getBalance();
        assertEquals(expResult, result, 0.0);
    }    
}
