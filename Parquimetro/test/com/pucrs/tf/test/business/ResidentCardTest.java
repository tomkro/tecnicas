package com.pucrs.tf.test.business;


import com.pucrs.tf.business.ConfigFile;
import com.pucrs.tf.business.ResidentCard;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author guilherme_tomas
 */
public class ResidentCardTest {
    
    
    @Before
    public void setUp() throws Exception {
        ConfigFile.getInstance("configuracoes1.txt");
    }
    
    /**
     * Método para testar identificador do cartão.
     */
    @Test
    public void testGetNumero() {
        System.out.println("getNumber");
        ResidentCard instance = new ResidentCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        String expResult = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        String result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Método para testar identificador do cartão.
     */
    public void testSetNumero() {
        System.out.println("setNumber");
        String id = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
        ResidentCard instance = new ResidentCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        String expResult = "123";
        instance.setId(expResult);
        assertEquals(expResult, instance.getId());
    }

    /**
     * Método para testar nome no cartão.
     */
    public void testSetGetName() {
        System.out.println("getName");
        ResidentCard instance = new ResidentCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        String expResult = "Tomás Kroth";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    }


    /**
     * Método para testar isenção.
     */
    @Test
    public void testGetIsencao() {
        System.out.println("getExemption");
        ResidentCard instance = new ResidentCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        String expResult = "S";
        instance.setExemption("S");
        String result = instance.getExemption();
        assertEquals(expResult, result);
    }

    /**
     * Método para testar isenção.
     */
    @Test
    public void testSetIsencao() {
        System.out.println("setExemption");
        ResidentCard instance = new ResidentCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        String expResult = "S";
        instance.setExemption("S");
        String result = instance.getExemption();
        assertEquals(expResult, result);
    }

    /**
     * Método para testar pagamento.
     */
    @Test
    public void testPagar() {
        System.out.println("doPayment");
        ResidentCard instance = new ResidentCard("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 10.0);
        instance.doPayment();
        assertEquals("S", instance.getExemption());
    }   
}
