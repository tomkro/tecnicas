package com.pucrs.tf.business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
* @author Guilherme_Tomas
 * Classe utilizada para formatação e padronização
 */
public class Helper {
    
    public static Helper h;
    
    private Helper(){}
    
    public static Helper getInstance(){
        if(Helper.h == null)
            Helper.h = new Helper();
        
        return Helper.h;
    }
    
    public java.sql.Date formatDate(String d) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(d);
            String output = dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }        
        return this.convertToSqlDate(date);
    }
    
    public java.sql.Date convertToSqlDate(Date d){
        java.util.Date utilDate = d;
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }
    
    public String getRandomHexString(){
        int numchars = 128;
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while(sb.length() < numchars){
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, numchars);
    }
    
    public void setPopUp(Object infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public String toDecimalFormat(double data, String format){
        String priceString = new DecimalFormat(format).format(data);
        return priceString;
    }
    
    public String formatDateTime(String date){
        String[] data_hora = date.split(" ");
        String[] data = data_hora[0].split("/");
        return data[2]+"-"+data[1]+"-"+data[0]+" "+data_hora[1];
    }
    
    public Ticket splitLog(String log){
        
        String[] dados = log.split(";");     
        
        Ticket ticket = new Ticket();
        ticket.setPaymentType(Integer.parseInt(dados[0]));
        ticket.setParkingMeterId(Integer.parseInt(dados[1]));
        ticket.setParkingMeterAddress(dados[2]);
        ticket.setEmissionDateTime(dados[3]);
        ticket.setValidToDateTime(dados[4]);
        ticket.setExemption(dados[5]);
        ticket.setValue(Double.parseDouble(dados[6]));
        
        return ticket;
    }
}
