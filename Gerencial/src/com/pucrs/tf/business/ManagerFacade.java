/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.business;

import com.pucrs.tf.persistence.DAOException;
import com.pucrs.tf.persistence.TicketDAO;
import com.pucrs.tf.persistence.TicketDAODerby;
import java.util.ArrayList;

/**
 *
* @author Guilherme_Tomas
 * Facade utilizado pro projeto gerencial
 */
public class ManagerFacade {
        
    /**
     * import dos tickets
     * @return ticket
     */
    public boolean importFile(ArrayList<String> insertLists){
        
        ArrayList<String> list = insertLists;
        TicketDAO ticketDB = new TicketDAODerby();
        boolean flag = false;        
        try {    
            for (String string : list) {
                flag = ticketDB.insertSQL(string);
            }
        } catch (DAOException e) {
            System.out.println(e.getMessage());
        }     
        return flag;
    }    
    
    /**
     * faz o import e insert dos tickets
     */
    public boolean importLogFile(ArrayList<Ticket> list){
        
        TicketDAO ticketDB = new TicketDAODerby();
        boolean flag = false;
        try {    
            for (Ticket ticket : list) {
                flag = ticketDB.insert(ticket);
            }
        } catch (DAOException e) {
            System.out.println(e.getMessage());
        }      
        return flag;
    }  
    
    /**
     * retorna o numero de clientes pagantes
     */
    private int getNumberOfPayers(String startDate, String endDate){
        
        TicketDAO ticketDB = new TicketDAODerby();
        int count = 0;     
        try {    
            count = ticketDB.getNumberOfPayers(startDate, endDate);
        } catch (DAOException e) {
            System.out.println(e.getMessage());
        }   
        return count;
    }  
    
    /**
     * retorna o numero de clientes isentos
     */
    private int getNumberOfExemptions(String startDate, String endDate){
        
        TicketDAO ticketDB = new TicketDAODerby();
        int count = 0;     
        try {    
            count = ticketDB.getNumberOfExemptions(startDate, endDate);
        } catch (DAOException e) {
            System.out.println(e.getMessage());
        }   
        return count;
    }  
    
    /**
     * retorna os dados das datas informadas
     */
    public ArrayList<String> getListByDate(String startDate, String endDate){
        
        TicketDAO ticketDB = new TicketDAODerby();
        ArrayList<String> list = new ArrayList<>();
        try {    
            list = ticketDB.getDatesList(startDate, endDate);
        } catch (DAOException e) {
            System.out.println(e.getMessage());
        }   
        return list;
    }  
    
    
    /**
     * retorna a contagem das datas
     */
    public ArrayList<Integer> getValueListByDate(ArrayList<String> listDate){
        
        TicketDAO ticketDB = new TicketDAODerby();
        ArrayList<Integer> list = new ArrayList<>();
        try {    
            for (String data : listDate) {
                list.add(ticketDB.getValuesListByDate(data));
            }
        } catch (DAOException e) {
            System.out.println(e.getMessage());
        }   
        return list;
    }  
    
    /**
     * retorna o percentual de clientes pagantes
     */    
    public double getPayersPercentage(String startDate, String endDate){
        double totalPayers = getNumberOfPayers(startDate, endDate);
        double totalExempts = getNumberOfExemptions(startDate, endDate);
        double percentage = (totalPayers / (totalPayers + totalExempts));
        return percentage;
    }
    
    /**
     * retorna o percentual de clientes isentos
     */    
    public double getExemptsPercentage(String startDate, String endDate){
        double totalPayers = getNumberOfPayers(startDate, endDate);
        double totalExempts = getNumberOfExemptions(startDate, endDate);
        double percentage = (totalExempts / (totalPayers + totalExempts));
        return(percentage);
    }
    
}
