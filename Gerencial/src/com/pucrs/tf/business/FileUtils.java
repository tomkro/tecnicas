package com.pucrs.tf.business;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
* @author Guilherme_Tomas
 */
public class FileUtils {
    public String narq;
      
    public FileUtils(String narq) {
        this.narq = narq;
    }
   
    /**
     * Lê e faz o insert dos .txt
     */
    public ArrayList<String> readInsertFile() {
        Path path = Paths.get(this.narq);
        ArrayList<String> list = new ArrayList<>();
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.defaultCharset()))) {
            sc.useDelimiter("\r\n");        
            while (sc.hasNext()) {     
                list.add(sc.next());
            }
            sc.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }
    
    /**
     * leitura dos logs 
     */
    public ArrayList<Ticket> readLogFile() {
        
        Path path = Paths.get(this.narq);
        ArrayList<Ticket> list = new ArrayList<>();
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.defaultCharset()))) {
            sc.useDelimiter("\r\n");   
            while (sc.hasNextLine()) {
                list.add(Helper.getInstance().splitLog(sc.nextLine()));
            }
            sc.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }
}
