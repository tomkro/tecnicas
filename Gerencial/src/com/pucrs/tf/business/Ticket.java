/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.business;

/**
 *
* @author Guilherme_Tomas
 */
public class Ticket {
    
    private int serialNumber;
    private int paymentType;
    private int parkingMeterId;
    private String parkingMeterAddress;
    private String emissionDateTime;
    private String validToDateTime;
    private String exemption;
    private double value;

    //retorna o serial
    public int getSerialNumber() {
        return serialNumber;
    }
    
    //seta o serial do ticket
    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    //informa o tipo de pagamento do ticket
    public int getPaymentType() {
        return paymentType;
    }

    //seta o tipo de pagamento do ticket
    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    //informa o id to parquimetro
    public int getParkingMeterId() {
        return parkingMeterId;
    }

    //seta o id do parquimetro
    public void setParkingMeterId(int parkingMeterId) {
        this.parkingMeterId = parkingMeterId;
    }

    //informa o endereço do parquimetro
    public String getParkingMeterAddress() {
        return parkingMeterAddress;
    }

    //seta o endereço do parquimetro
    public void setParkingMeterAddress(String parkingMeterAddress) {
        this.parkingMeterAddress = parkingMeterAddress;
    }

    //informa a hora que o ticket foi emitido
    public String getEmissionDateTime() {
        return emissionDateTime;
    }

    //seta a hora da emissão do ticket
    public void setEmissionDateTime(String emissionDateTime) {
        this.emissionDateTime = emissionDateTime;
    }

    //informa a validade do ticket
    public String getValidToDateTime() {
        return validToDateTime;
    }

    //seta a validade do ticket
    public void setValidToDateTime(String validToDateTime) {
        this.validToDateTime = validToDateTime;
    }

    //informa se é isento
    public String getExemption() {
        return exemption;
    }

    //seta a isenção
    public void setExemption(String exemption) {
        this.exemption = exemption;
    }

    //informa o value pago
    public double getValue() {
        return value;
    }

    //seta o value
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Ticket{" + "serialNumber=" + serialNumber + ", paymentType=" + paymentType + ", parkingMeterId=" + parkingMeterId + ", parkingMeterAddress=" + parkingMeterAddress + ", emissionDateTime=" + emissionDateTime + ", validToDateTime=" + validToDateTime + ", exemption=" + exemption + ", value=" + value + '}';
    }
}
