/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.business;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
* @author Guilherme_Tomas
 * classe utilizada pra fazer os graficos de pizza
 */
public class PizzaChart {
    
    public JLabel label;
    public JLabel name;
    public JButton btn;
    public JFreeChart chart;
    public static DefaultPieDataset pieDataSet;
    public static PizzaChart grafico;

    private PizzaChart(){}
    
    public static PizzaChart getInstance() {
        if(grafico == null){
            grafico = new PizzaChart();
            pieDataSet = new DefaultPieDataset();
        }
        return grafico;
    }
    
    
    
    public void botaoBarraActionPerformed(java.awt.event.ActionEvent evt) {  
        label = new JLabel("Notes");
        name = new JLabel("Notes, 1.23");
        name.setFont(new Font("Serif", Font.BOLD, 13));
        btn = new JButton("OK");
    }                                          

    /**
     * Inicio grafico pizza
     *
     */
    private PieDataset createDataset(double totalPagantes, double totalIsentos) {
        pieDataSet = new DefaultPieDataset();;
        pieDataSet.setValue("Pagantes", new Double(totalPagantes));
        pieDataSet.setValue("Isentos", new Double(totalIsentos));
        return pieDataSet;
    }

    private JFreeChart setPizzaChart(PieDataset dataset, String tituloGrafico) {
        JFreeChart chart = ChartFactory.createPieChart(tituloGrafico, pieDataSet, true, true, false);
        return chart;
    }

    public JPanel createPizzaChart(String tituloGrafico, double totalPagantes, double totalIsentos) {
        chart = setPizzaChart(createDataset(totalPagantes, totalIsentos), tituloGrafico);
        return new ChartPanel(chart);
    }
}
