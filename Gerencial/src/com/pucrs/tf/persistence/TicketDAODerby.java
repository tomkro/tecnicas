package com.pucrs.tf.persistence;

import com.pucrs.tf.business.Ticket;

import java.sql.*;
import java.util.ArrayList;

public class TicketDAODerby implements TicketDAO {

    @Override
    public boolean insert(Ticket t) throws DAOException {
        String sql = "INSERT INTO Ticket (TIPO_PAGAMENTO, ID_PARQUIMETRO, ENDERECO_PARQUIMETRO, DATA_EMISSAO, DATA_VALIDADE, ISENTO, VALOR) VALUES(?,?,?,?,?,?,?)";
        boolean flag = true;
        try (Connection conexao = DBInitiliazer.getConnectionThroughDS()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, t.getPaymentType());
                comando.setInt(2, t.getParkingMeterId());
                comando.setString(3, t.getParkingMeterAddress());
                comando.setString(4, t.getEmissionDateTime());
                comando.setString(5, t.getValidToDateTime());
                comando.setString(6, t.getExemption());
                comando.setDouble(7, t.getValue());
                comando.executeUpdate();
            }
        } catch (Exception ex) {
            flag = false;
            ex.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean insertSQL(String exec) throws DAOException {
        String sql = exec;
        boolean flag = true;
        try (Connection conexao = DBInitiliazer.getConnectionThroughDS()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.executeUpdate();
            }
        } catch (Exception ex) {
            flag = false;
            ex.printStackTrace();            
        }
        return flag;
    }

    @Override
    public int getNumberOfExemptions(String dataInicio, String dataFim) throws DAOException {
        ArrayList<Integer> lista = new ArrayList<Integer>();
        try (Connection conexao = DBInitiliazer.getConnectionThroughDS()) {
            String sql = "SELECT NUMERO_SERIAL FROM Ticket WHERE ISENTO = 'S'";
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultados = comando.executeQuery()) {
                    while (resultados.next()) {
                        lista.add(resultados.getInt("NUMERO_SERIAL"));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return lista.size();
    }

    @Override
    public int getNumberOfPayers(String dataInicio, String dataFim) throws DAOException {
        ArrayList<Integer> lista = new ArrayList<Integer>();
        try (Connection conexao = DBInitiliazer.getConnectionThroughDS()) {
            String sql = "SELECT NUMERO_SERIAL FROM Ticket WHERE ISENTO != 'S'";
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultados = comando.executeQuery()) {
                    while (resultados.next()) {
                        lista.add(resultados.getInt("NUMERO_SERIAL"));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return lista.size();
    }

    @Override
    public ArrayList<String> getDatesList(String startDate, String endDate) throws DAOException {
        ArrayList<String> list = new ArrayList<String>();
        try (Connection conn = DBInitiliazer.getConnectionThroughDS()) {
            //Consultar dados da tabela
            String sql = "SELECT DATA_EMISSAO FROM Ticket WHERE DATA_EMISSAO BETWEEN ? AND ? GROUP BY DATA_EMISSAO";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, startDate);
                stmt.setString(2, endDate);
                try (ResultSet resultados = stmt.executeQuery()) {
                    while (resultados.next()) {
                        list.add(resultados.getString("DATA_EMISSAO"));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @Override
    public int getValuesListByDate(String date) throws DAOException {
        ArrayList<Integer> list = new ArrayList<Integer>();
        try (Connection conn = DBInitiliazer.getConnectionThroughDS()) {
            String sql = "SELECT NUMERO_SERIAL FROM Ticket WHERE DATA_EMISSAO = ?";
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, date);
                try (ResultSet resultados = stmt.executeQuery()) {
                    while (resultados.next()) {
                        list.add(resultados.getInt("NUMERO_SERIAL"));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list.size();
    }
}
