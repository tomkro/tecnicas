package com.pucrs.tf.persistence;

import org.apache.derby.jdbc.ClientDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.sql.DataSource;

public class DBInitiliazer {

    public static String DB_CONN_STRING_CREATE = "jdbc:derby://localhost:1527/TrabalhoFinal";
    public static String DB_NAME = "TrabalhoFinal";
    public static String USER_NAME = "usuario";
    public static String PASSWORD = "senha";
    private static DataSource dataSource;

    public static Connection getConnectionThroughDM() throws Exception {
        return DriverManager.getConnection(DB_CONN_STRING_CREATE, USER_NAME, PASSWORD);
    }

    public static Connection getConnectionThroughDS() throws Exception {
        if (dataSource == null) {
            ClientDataSource ds = new ClientDataSource();
            ds.setDatabaseName(DB_NAME);
            ds.setCreateDatabase("create");
            ds.setUser(USER_NAME);
            ds.setPassword(PASSWORD);
            dataSource = ds;
        }
        return dataSource.getConnection();
    }

}
