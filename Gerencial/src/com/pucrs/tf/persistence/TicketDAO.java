/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pucrs.tf.persistence;

import com.pucrs.tf.business.Ticket;

import java.util.ArrayList;

/**
 *
* @author Guilherme_Tomas
 */
public interface TicketDAO {
    boolean insert(Ticket c) throws DAOException;
    boolean insertSQL(String exec) throws DAOException;
    int getNumberOfPayers(String dataInicio, String dataFim) throws DAOException;
    int getNumberOfExemptions(String dataInicio, String dataFim) throws DAOException;
    ArrayList<String> getDatesList(String dataInicio, String dataFim) throws DAOException;
    int getValuesListByDate(String data) throws DAOException;
}
