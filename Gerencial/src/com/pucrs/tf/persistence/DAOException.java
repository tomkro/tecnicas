

package com.pucrs.tf.persistence;

/**
 *
* @author Guilherme_Tomas
 */
public class DAOException extends Exception {

    public DAOException(String message) {
        super(message);
    }

}
